<?php

namespace app\modules\MubAdmin\modules\gallery\controllers;

use Yii;
use app\models\MubUserAlbum;
use app\models\MubUserAlbumSearch;
use app\modules\MubAdmin\modules\gallery\models\GalleryProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class AlbumController extends MubController
{
    public function getProcessModel()
    {
        return new GalleryProcess();
    }

    public function getPrimaryModel()
    {
        return new MubUserAlbum();
    }

    public function getSearchModel()
    {
        return new MubUserAlbumSearch();
    }

}