<?php 
namespace app\modules\MubAdmin\modules\yoga\models;

use app\components\Model;

class CategoryProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $category = new Category();
        $this->models = [
            'category' => $category
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $category = $model;
        $this->relatedModels = [
            'category' => $category
        ];
        return $this->relatedModels;
    }

    public function saveCategory($category)
    {
        $userId = \app\models\User::getMubUserId();
        $category->mub_user_id =  $userId;
    	return ($category->save()) ? $category->id : p($category->getErrors());
    }

    public function saveData($data)
    {
    	if(isset($data['category']))
        {
        try {
        	$catId = $this->saveCategory($data['category']);
        	return ($catId) ? $catId : false;  
        	}
        	catch (\Exception $e)
            {
                throw $e;
            }
        }
    	throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
