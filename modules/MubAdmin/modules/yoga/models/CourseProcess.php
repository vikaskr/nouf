<?php 
namespace app\modules\MubAdmin\modules\yoga\models;

use app\components\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii;

class CourseProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $course = new Course();
        $courseCategories = new CourseCategory();
        $this->models = [
            'course' => $course,
            'courseCategories' => $courseCategories
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $categories = new Category();
        $condition = ['status' => 'active'];
        $allCategories = $categories->getAll('category_name',$condition);
        return [
            'allCategories' => $allCategories
        ];
    }

    public function getSavedCategory($category)
    {
        $categories = [];
        foreach ($category as $key => $amen) 
        {
            $categories[] = $amen->category_id;
        }

        return $categories;
    }

    public function getRelatedModels($model)
    {
       $courseSelectedCategories = $this->getSavedCategory($model->courseCategories);

        if(!empty($courseSelectedCategories))
        {
            $courseCategories = $model->courseCategories[0];
            
            if(count($courseSelectedCategories) == '1')
            {
                $courseCategories->category_id = $courseSelectedCategories[0];
            }
            else
            {
                $courseCategories->category_id = $courseSelectedCategories;            
            }
        }
        else
        {
            $courseCategories = new CourseCategory();
        }

        $this->relatedModels = [
            'course' => $model,
            'courseCategories' => $courseCategories
        ];
        return $this->relatedModels;
    }

   public function saveImage($course)
    {
        $course->image = UploadedFile::getInstance($course,'image');
        $imageHelper = new \app\helpers\ImageUploader();
        $success = $imageHelper::uploadImages($course,'image');
        return true;
    }

    public function saveCourse($course)
    {
        $image = UploadedFile::getInstance($course,'image');
        if($image)
        {
            $this->SaveImage($course);
        }
        else 
        {
            if(\Yii::$app->controller->action->id == 'update')
            {    
                $courseModel = new Course();
                $image = $courseModel->findOne($course->id)->image;
                $course->image = $image;
            }
        }
        $course->slug = StringHelper::generateSlug($course->name);
        return ($course->save()) ? $course->id : p($course->getErrors());
    }

    public function saveCourseCategories($courseCategories,$courseId)
    {
        $selectedCategories = $courseCategories->category_id;
        $mubUserId = \app\models\User::getMubUserId();
        $attribs = ['mub_user_id','course_id','category_id','created_at'];
        $recordSet = [];
        foreach ($selectedCategories as $categoryId) {
            $recordSet[] = [$mubUserId,$courseId,$categoryId,date('Y-m-d h:m:s',time())];
        }
        $amenCount = count($selectedCategories);

        $courseCategories->deleteAll(['course_id' => $courseId]);
        $inserted = Yii::$app->db->createCommand()->batchInsert($courseCategories->tableName(), $attribs, $recordSet)->execute();
        if($inserted == $amenCount)
        {
            return true;
        }
        p($courseCategories->getErrors());
    }


    public function saveData($data = [])
    {
    	 if (isset($data['course'])&&
            isset($data['courseCategories']))
            {
            try {
                   $courseId = $this->saveCourse($data['course']);
                   if($courseId)
                    {
                        $categories = $this->saveCourseCategories($data['courseCategories'],$courseId);
                        if($categories)
                        {
                            return $courseId;
                        }
                        p($data['courseCategories']->getErrors());
                    }
                    p($data['course']->getErrors());
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}
