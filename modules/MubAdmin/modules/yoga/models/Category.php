<?php

namespace app\modules\MubAdmin\modules\yoga\models;

use Yii;
use app\models\MubUser;
/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $mub_user_id
 * @property string $category_name
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 *
 * @property MubUser $mubUser
 */
class Category extends \app\components\Model
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mub_user_id'], 'integer'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['category_name'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'category_name' => 'Category Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
