<?php

namespace app\modules\MubAdmin\modules\yoga\models;

use Yii;
use app\models\MubUser;
/**
 * This is the model class for table "course_category".
 *
 * @property int $id
 * @property int $mub_user_id
 * @property int $course_id
 * @property int $category_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 *
 * @property Category $category
 * @property Course $course
 * @property MubUser $mubUser
 */
class CourseCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at','category_id','course_id','mub_user_id'], 'safe'],
            [['del_status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'course_id' => 'Course ID',
            'category_id' => 'Category ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
