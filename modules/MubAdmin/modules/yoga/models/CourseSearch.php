<?php

namespace app\modules\MubAdmin\modules\yoga\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\yoga\models\Course;

/**
 * CourseSearch represents the model behind the search form of `app\modules\MubAdmin\modules\yoga\models\Course`.
 */
class CourseSearch extends Course
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id'], 'integer'],
            [['name', 'desciption', 'image', 'duration', 'open_date', 'no_of_week', 'additional_info', 'status', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Course::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'desciption', $this->desciption])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'duration', $this->duration])
            ->andFilterWhere(['like', 'open_date', $this->open_date])
            ->andFilterWhere(['like', 'no_of_week', $this->no_of_week])
            ->andFilterWhere(['like', 'additional_info', $this->additional_info])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
