<?php

namespace app\modules\MubAdmin\modules\yoga\models;

use Yii;
use app\models\MubUser;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property int $mub_user_id
 * @property string $name
 * @property string $desciption
 * @property string $image
 * @property string $duration
 * @property string $open_date
 * @property string $no_of_week
 * @property string $additional_info
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 *
 * @property MubUser $mubUser
 * @property CourseCategory[] $courseCategories
 */
class Course extends \app\components\Model
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mub_user_id'], 'integer'],
            [['desciption', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name','slug'], 'string', 'max' => 50],
            [['image', 'duration', 'open_date', 'no_of_week', 'additional_info'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'name' => 'Name',
            'desciption' => 'Desciption',
            'image' => 'Image',
            'duration' => 'Duration',
            'open_date' => 'Open Date',
            'no_of_week' => 'No Of Week',
            'additional_info' => 'Additional Info',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseCategories()
    {
        return $this->hasMany(CourseCategory::className(), ['course_id' => 'id']);
    }
}
