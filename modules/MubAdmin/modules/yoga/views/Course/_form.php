<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\yoga\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($course, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($course, 'desciption')->textarea(['rows' => 5]) ?>

    <?= $form->field($course, 'image')->fileInput()  ?>

    <?= $form->field($course, 'duration')->textInput(['maxlength' => true]) ?>

    <?= $form->field($course, 'open_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($course, 'no_of_week')->textInput(['maxlength' => true]) ?>

    <?= $form->field($course, 'additional_info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($course, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?>
    <?php echo $form->field($courseCategories, 'category_id')->checkboxList($allCategories)->label('Category'); ?></div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
<br/>
    <?php ActiveForm::end(); ?>

</div>
