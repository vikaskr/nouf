<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\yoga\models\Course */

$this->title = $course->name;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $course->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $course->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $course,
        'attributes' => [
            'id',
            'mub_user_id',
            'name',
            'desciption:ntext',
            'image',
            'duration',
            'open_date',
            'no_of_week',
            'additional_info',
            'status',
            'created_at',
            'updated_at',
            'del_status',
        ],
    ]) ?>

</div>
