<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $course app\modules\MubAdmin\modules\yoga\courses\CourseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($course, 'id') ?>

    <?= $form->field($course, 'mub_user_id') ?>

    <?= $form->field($course, 'name') ?>

    <?= $form->field($course, 'desciption') ?>

    <?= $form->field($course, 'image') ?>

    <?php // echo $form->field($course, 'duration') ?>

    <?php // echo $form->field($course, 'open_date') ?>

    <?php // echo $form->field($course, 'no_of_week') ?>

    <?php // echo $form->field($course, 'additional_info') ?>

    <?php // echo $form->field($course, 'status') ?>

    <?php // echo $form->field($course, 'created_at') ?>

    <?php // echo $form->field($course, 'updated_at') ?>

    <?php // echo $form->field($course, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
