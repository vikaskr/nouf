<?php

namespace app\modules\MubAdmin\controllers;

use Yii;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\City;
use app\models\MubUser;
use yz\shoppingcart\ShoppingCart;
use app\models\HomesListing;
use app\components\Model;
use yii\helpers\ArrayHelper;

class DashboardController extends MubController
{
	public function getProcessModel()
	{

	}

	public function getPrimaryModel()
	{

	}

	public function getDataProviders()
	{
		return [];
	}

    public function saveBookingDetails($postData)
    {
         $cart = new ShoppingCart();
         $cartItems = $cart->getPositions();
         $price = $cart->getCost();

        if(!empty($postData))
        {
            $mubUserId = \app\models\User::getMubUserId();
            $booking = new \app\models\Booking();
            $booking->name = $postData['firstname'].' '.$postData['lastname'];
            $booking->mub_user_id = $mubUserId;
            $booking->address = $postData['address'];
            $booking->address_line_2 = $postData['address_line_2'];
            $booking->time = $postData['time'];
            $booking->email = $postData['email'];
            $booking->mobile = $postData['mobile'];
            $booking->amount = $postData['amount'];
            $booking->transaction_id = $postData['txnid'];
            $booking->city = $postData['city'];
            $booking->country = $postData['country'];
           

            if(!$booking->save())
            {
               p($booking->getErrors());
            }

           $success = \Yii::$app->mailer->compose('Listhomemail')
                    ->setFrom('info@mayaexpress.in')
                    ->setCc('praveen@makeubig.com')
                    ->setBcc('info@mayaexpress.in','MayaExpress ADMIN')
                    ->setTo($booking->email)->setSubject('Your Booked Order')
                    ->send();    

                return $success;
        }
        return true;
    }

    

	public function actionGetCity() {
        if (Yii::$app->request->isAjax) {
            $stateId = Yii::$app->request->post('stateId');
            // $data = [];
            $result = City::find()->where(['state_id' => $stateId])->all();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'result' => $result
            ];
        } else {
            return false;
        }
    }

    public function actionSetAttribute()
    {
        if (Yii::$app->request->isAjax) 
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $params = \Yii::$app->request->getBodyParams();
            $model = new $params['model']();
            $dataModel = $model::findOne($params['id']);
            if($params['model'] == '\\app\\models\\User')
            {
                $mubUser = MubUser::findOne($params['id']);
                $dataModel = $model::find()->where(['id' => $mubUser->user_id,'del_status' => '0'])->one();
            }
            $dataModel->{$params['attribute']} = $params['value'];
            $response['message'] = 'Data Updated successfully';
            if(!$dataModel->save(false))
            {
                $key = array_keys($dataModel->getErrors());
                $response['message'] = $dataModel->getErrors()[$key];
            }
            return $response;
        } 
        else 
        {
            return false;
        }

    }

    public function actionListHome()
    {
        
        $formData = \Yii::$app->request->getBodyParams();
        if(isset($formData['HomesListing']))
        {
            $listHome = new \app\models\HomesListing();
            if($listHome->load($formData))
            {
                if(!$listHome->save())
                {
                    p($listHome->getErrors());
                }
                // $homelistModel = new \app\modules\MubAdmin\modules\hotels\models\Property();
                // $listhome = $homelistModel::findOne($listHome['mub_user_id']);
               
                \Yii::$app->mailer->compose('Listhomemail',['listHome' => $listHome])
                    ->setFrom('info@osmstays.com')
                    ->setCc('info@osmstays.com')
                    ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                    ->setTo([$formData['HomesListing']['email'],$listHome->email])->setSubject('Your Listed Home')
                    ->send();       
                return $listHome->id;
            }
        }
       
        return false;
    }

    public function actionPayuHash()
    {
        if(\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
            $postData = \Yii::$app->request->getBodyParams();
            $hashVarsSeq = explode('|', $hashSequence);
            $hash_string = '';  
              foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($postData[$hash_var]) ? $postData[$hash_var] : '';
                $hash_string .= '|';
            }
            //add merchant salt here
          $hash_string .= '1aqaXEeH0e';
          $result['hash'] = strtolower(hash('sha512', $hash_string));
          $this->saveBookingDetails($postData);
          return $result;
        }
    }

}