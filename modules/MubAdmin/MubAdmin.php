<?php

namespace app\modules\MubAdmin;

class MubAdmin extends \app\components\Module
{
    public $controllerNamespace = 'app\modules\MubAdmin\controllers';
    public $defaultRoute = 'dashboard';
    public function init()
    {
        parent::init();

        $this->modules = [
        'gallery' => [
            'class' => 'app\modules\MubAdmin\modules\gallery\Gallery',
            ],
        'yoga' => [
            'class' => 'app\modules\MubAdmin\modules\yoga\Yoga',
            ],
        'users' => [
            'class' => 'app\modules\MubAdmin\modules\users\User',
            ],
        ];
    }
}
