<?php

function p($data){
  echo '<pre>';
  die(print_r($data));
}
$conn = mysqli_connect("localhost","root","","foodonline");

if ($conn->connect_error) {
    die("Connection failed: " . $con->connect_error);
} 

if(isset($_POST["txnid"]))
{
$txnid=$_POST["txnid"];


if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$sql = "UPDATE booking SET status='done' WHERE booking_id='$txnid'";

if ($conn->query($sql) === TRUE) {
    echo "";
} else {
    echo "Error updating record: " . $conn->error;
}
}
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
  <title>Nouf Yoga Acedemy</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="icon" href="image/favicon.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="/css/vendor.bundle.css">
  <link id="style-css" rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body class="site-body style-v1">
  <!-- Header --> 
  <header class="site-header header-s1 is-transparent is-sticky">
    <!-- Topbar -->
    <div class="topbar">
      <div class="container">
        <div class="row">
          <div class="top-aside top-left">
            <ul class="top-nav">
              <li><a href="#">On Press</a></li>
              <li><a href="#">Career</a></li>
              <li><a href="#">Our Offices</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>
          <div class="top-aside top-right clearfix">
            <ul class="top-contact clearfix">
              <li class="t-email t-email1">
                <em class="fa fa-envelope-o" aria-hidden="true"></em>
                <span><a href="#">noufacademy@gmail.com</a></span>
              </li>
              <li class="t-phone t-phone1">
                <em class="fa fa-phone" aria-hidden="true"></em>
                <span>011 - 49842280</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- #end Topbar -->
    <!-- Navbar -->
    <div class="navbar navbar-primary">
      <div class="container">
          <a class="navbar-brand" href="./">
                    <h2 style="color: #eca116;">Nouf Yogashala</h2>
                </a>
        <!-- Logo -->
        <a class="navbar-brand" href="./">
          <img class="logo logo-dark" alt="" src="image/logo.png" srcset="image/logo2x.png 2x">
          <img class="logo logo-light" alt="" src="image/logo-light.png" srcset="image/logo-light2x.png 2x">
        </a>
        <!-- #end Logo -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainnav" aria-expanded="false">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- Q-Button for Mobile -->
          <div class="quote-btn"><a class="btn" href="get-a-quote.html">Sign in</a></div>
        </div>
        <!-- MainNav -->
        <nav class="navbar-collapse collapse" id="mainnav">
          <ul class="nav navbar-nav">
            <li class="active"><a href="./">Home</a>
             <!--  <ul class="dropdown-menu">
                <li><a href="index.html">Home - Default</a></li>
                <li><a href="index-v2.html">Home - Version 2</a></li>
                <li><a href="index-v3.html">Home - Version 3</a></li>
                <li><a href="index-v4.html">Home - Version 4</a></li>
                <li><a href="index-static.html">Home - Image</a></li>
              </ul> -->
            </li>
            <!-- <li class="dropdown"><a href="about-us.html">About us <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="faqs.html">FAQ's</a></li>
                <li><a href="teams.html">Teams</a></li>
                <li><a href="about-us-alter.html">About - Alter</a></li>
                <li><a href="teams-alter.html">Teams - Alter</a></li>
                <li><a href="contact-extend.html">Contact - Extend</a></li>
                <li><a href="shortcode.html">Shortcode <span class="label label-danger">Hot</span></a></li>
                <li><a href="typography.html">Typography</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="service.html" class="dropdown-toggle">Services <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="service.html">Services List</a></li>
                <li><a href="service-alter.html">Services List - Alter</a></li>
                <li><a href="service-single.html">Services Single</a></li>
                <li><a href="service-single-alter.html">Services Single - Alter</a></li>
              </ul>
            </li>
            <li><a href="testimonial.html">Testimonial</a></li>
            <li><a href="news.html">News</a></li>
            <li class="quote-btn"><a class="btn" href="contact.html">Contact</a></li> -->
          </ul>
        </nav>     
        <!-- #end MainNav -->
      </div>
    </div>
    <!-- #end Navbar -->
</header>

  <!-- Call Action --><br/><br/><br/><br/>

  <div class="container">
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <img class="img-responsive" src="./success_asset/success.png" alt="w3ls" style="margin-left: 50px; margin-top: 20px; margin-bottom: 30px;" height="120" style="height: 120px!important;">   
       <h4 style="text-align: center;"><b>Thank You !</b></h4><br>
       <h4 style="text-align: center;"><b>Your transaction complete.</b></h4><br>
       
   </div>
</div>

</div>
     <div class="call-action cta-small has-bg bg-primary" style="background-image: url('/images/banner.png');">
        <div class="cta-block">
            <div class="container">
                <div class="content row">

                    <div class="cta-sameline">
                        <h2>Have any Question?</h2>
                        <p>Get on touch with us on facebook / Instagram/ Twitter Search NAcademy</p>
                        <a class="btn btn-alt" href="#">Contact Us</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End Section -->

    <!-- Footer Widget-->
    <div class="footer-widget style-v2 section-pad-md">
        <div class="container">
            <div class="row">

                <div class="widget-row row">
                    <div class="footer-col col-md-3 col-sm-6 res-m-bttm">
                        <!-- Each Widget -->
                        <div class="wgs wgs-footer wgs-text">
                            <div class="wgs-content">
                                <p><img src="/images/logo.png" srcset="/images/logo2x.png 2x" alt=""></p>
                                <p>Our Courses are specially designed to expand the knowledge of yoga teachers & health practitioners to learn more in the field of Indian Medicine, Philosophy & Yoga. please request us at <a href="mailto:noufacademy@gmail.com">noufacademy@gmail.com</a> to know more...  </p>
                            </div>
                        </div>
                        <!-- End Widget -->
                    </div>
                    <div class="footer-col col-md-3 col-sm-6 col-md-offset-1 res-m-bttm">
                        <!-- Each Widget -->
                        <div class="wgs wgs-footer wgs-menu">
                            <h5 class="wgs-title">Our Courses</h5>
                            <div class="wgs-content">
                                <ul class="menu">
                                    <li><a href="#">Level1 Yoga teacher training course</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Widget -->
                    </div>
                    <div class="footer-col col-md-2 col-sm-6 res-m-bttm">
                        <!-- Each Widget -->
                        <div class="wgs wgs-footer wgs-menu">
                            <h5 class="wgs-title">Quick Links</h5>
                            <div class="wgs-content">
                                <ul class="menu">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Testimonial</a></li>
                                    <li><a href="#">Expert</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Widget -->
                    </div>

                    <div class="footer-col col-md-3 col-sm-6">
                        <!-- Each Widget -->
                        <div class="wgs wgs-footer">
                            <h5 class="wgs-title">Get In Touch</h5>
                            <div class="wgs-content">
                                <p>
                                    317,318.<br />
                                    Third floor, Plot no. 9 <br />
                                    Vikas Surya Galaxy, Sector 4,<br />
                                    Dwarka, New Delhi - 110075
                                <p><span>Toll Free</span>: 011 - 49842280<br>
                                    <span>Phone</span>: +91 - 8111811891 </p>
                                <ul class="social">
                                    <li><a href="#"><em class="fa fa-facebook" aria-hidden="true"></em></a></li>
                                    <li><a href="#"><em class="fa fa-twitter" aria-hidden="true"></em></a></li>
                                    <li><a href="#"><em class="fa fa-linkedin" aria-hidden="true"></em></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Widget -->
                    </div>

                </div><!-- Widget Row -->

            </div>
        </div>
    </div>
    <!-- End Footer Widget -->
  <!-- End Footer Widget -->

  <!-- Copyright -->
  <div class="copyright style-v2">
    <div class="container">
      <div class="row">
      
        <div class="row">
          <div class="site-copy col-sm-7">
            <p>&copy; 2017 Nouf Academy. <a href="#">Policy</a></p>
          </div>
          <div class="site-by col-sm-5 al-right">
            <p>Template Made by <a href="http://makeubig.com/" target="_blank">MakeUBIG.</a></p>
          </div>
        </div>
                
      </div>
    </div>
  </div>
  <!-- End Copyright -->
  
  <!-- Rreload Image for Slider -->
  <div class="preload hide">
    <img alt="" src="image/slider-lg-a.jpg">
    <img alt="" src="image/slider-lg-b.jpg">
  </div>
  <!-- End -->

  <!-- Preloader !active please if you want -->
  <!-- <div id="preloader"><div id="status">&nbsp;</div></div> -->
  <!-- Preloader End -->

  <!-- JavaScript Bundle -->
  <script src="js/jquery.bundle.js"></script>
  <!-- Theme Script init() -->
  <script src="js/script.js"></script>
  <!-- End script -->
</body>
</html>