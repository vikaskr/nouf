<?php 

namespace common\helpers;

class MailHelper
{
	public static function sendMail($from,$to,$subject,$layout,$formvars)
	{
		$mail = \Yii::$app->mailer->compose()
		->setFrom($from)
		->setTo($to)
		->setSubject($subject);
		if(!is_file($layout)){
			$mail->setHtmlBody($layout);
		}
	}

}