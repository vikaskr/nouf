<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\MubUser;
use app\models\ClientSignup;
use app\modules\MubAdmin\modules\hotels\models\Restaurant;
use app\modules\MubAdmin\modules\hotels\models\Item;
use app\modules\MubAdmin\modules\hotels\models\Menu;
use yz\shoppingcart\ShoppingCart;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authSuccess'],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->view->params['page']='home';
        return $this->render('index');
    }


    public function actionRestaurantList($name)
    {
        $this->view->params['page'] = 'restaurantlist';
        if($name !== '')
        {
            $restaurantModel = new Restaurant();
            $restaurant = $restaurantModel::find()->where(['restaurant_slug' => $name])->one();
            $menus = $restaurant->menus;
        }
        return $this->render('restaurantlist',['menus' => $menus,'restaurant' => $restaurant]);
    }

    public function actionOrder($name,$item)
    {
        $this->view->params['page']='restaurantlist';
        if($name !== '')
        {
            $restaurantModel = new Restaurant();
            $restaurant = $restaurantModel::find()->where(['restaurant_slug' => $name])->one();
            $menus = $restaurant->menus;
        }
        $items = new Item();
        $itemAdd = $items::find()->where(['del_status' => '0', 'item_slug' => $item])->all();
        return $this->renderAjax('order',['menus' => $menus,'restaurant' => $restaurant, 'itemAdd' => $itemAdd]);
    }

    public function actionHelp()
    {
        $this->layout = false;
        return $this->renderAjax('help');
    }
    
    public function actionForgetpass()
    {
        $this->layout =false;
        if(Yii::$app->request->post())
        {
            $postData = \Yii::$app->request->post();
            $mubUserModel = new \app\models\MubUser();
            $mubUserContactModel = new \app\models\MubUserContact();
            $mubUserContact = $mubUserContactModel::find()->where(['email' => $postData['ClientSignup']['email']])->one();
            if(!empty($mubUserContact))
            {  
                $mubUser = $mubUserModel::find()->where(['id' => $mubUserContact->mub_user_id, 'del_status' => '0'])->one();
                $userEmail = $mubUserContact['email'];
                \Yii::$app->mailer->compose('forget',['mubUserContact' => $mubUserContact,'mubUser' => $mubUser])
                    ->setFrom('info@mayaexpress.in')
                    ->setTo($mubUserContact->email)
                    ->setCc('info@mayaexpress.in')
                    ->setSubject('Your Mayaexpress Password')->setBcc('praveen@makeubig.com','MakeUBIG ADMIN')->send();
                    return 'mailsent';
            }
        }
        return $this->render('forgetpass');
    }


    public function AuthSuccess($client) 
    {
        $userAttributes = $client->getUserAttributes();
        if($client instanceof yii\authclient\clients\Google)
        {
            if(!empty($userAttributes['emails']))
            {
                $currentMail = $userAttributes['emails']['0']['value'];    
            }
        }
        elseif($client instanceof yii\authclient\clients\Facebook)
        {
            $currentMail = $userAttributes['email'];
        }
        if($currentMail != '')
        {
            $userContact = new \app\models\MubUserContact();
            $userExists = $userContact::find()->where(['email' => $currentMail,'del_status' => '0','status' => 'Active'])->one();
            $model = new LoginForm();
            if(empty($userExists)){
                $clientModel = new \app\models\ClientSignup();
                if($client instanceof yii\authclient\clients\Google)
                {
                    if(!empty($userAttributes['emails']))
                    {
                        $clientModel->email = $userAttributes['emails']['0']['value'];
                        $clientModel->first_name = $userAttributes['name']['givenName']; 
                        $clientModel->last_name = $userAttributes['name']['familyName'];
                        $clientModel->username = $userAttributes['emails']['0']['value'];   
                    }
                }
                elseif($client instanceof yii\authclient\clients\Facebook)
                {
                   $clientModel->email = $userAttributes['email'];
                   $clientModel->first_name = $userAttributes['first_name'];
                   $clientModel->last_name = $userAttributes['last_name'];
                   $clientModel->username = $userAttributes['email'];
                }
                $clientModel->password = 'osmstays';
                $user  = $clientModel->signup();
                if(!empty($user))
                {
                    $model->username = $user->username;
                    $model->password = $user->password;
                    $model->rememberMe = 1;
                    if($model->login()) 
                    {
                        $mubUserId = \app\models\User::getMubUserId();
                        $mubUser = MubUser::findOne($mubUserId);
                        $mubUserContact = $mubUser->mubUserContacts;
                         \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                        ->setFrom('info@mayaexpress.in')
                        ->setTo($mubUserContact->email)
                        ->setSubject('Your Profile Created')
                        ->setCc('info@mayaexpress.in')
                        ->setBcc('praveen@makeubig.com','MakeUBIG ADMIN')
                        ->send();
                       return $this->redirect('/');
                    }
                    else
                   {
                    p(['here' => $model->getErrors()]);
                   }
                }
            }
            else
            {
                $mubUser = MubUser::find()->where(['id' => $userExists->mub_user_id])->one();
                if(!empty($mubUser))
                {
                    $model->username = $mubUser->username;
                    $model->password = $mubUser->password;
                    if($model->login()) {
                       return $this->redirect('/');
                    }
                    else
                    {
                        p('there was a problem Logging you in ! Please contact Support');
                    }
                }
               p('Username Not found In database'); 
            }
        }
        else
        {
            p('Mail Id not found in social account');
        }
    }
    

    

    public function updateUserRecord($postData)
    {
        $userModel = new \app\models\User();
        $mubUserModel = new \app\models\MubUser();
        $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
        $mubUserContact = $mubUser->mubUserContacts;
        $user = $userModel::findOne($mubUser->user_id);
        $user->first_name = $postData['MubUser']['first_name'];
        $user->last_name = $postData['MubUser']['last_name'];
        $user->setPassword($postData['MubUser']['password']);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        return ($user->save(false)) ? true : p($user->getErrors());
    }
    
    public function actionProfile()
    {   
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
        }
        else 
        {
            if(Yii::$app->request->post())
            {
                $postData = \Yii::$app->request->post();
                $userModel = new \app\models\User();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
                $user = $userModel::findOne($mubUser->user_id);
                $mubUserContact = $mubUser->mubUserContacts;
            
                if($mubUser->load($postData) && $mubUserContact->load($postData))
                {
                    if($mubUser->save(false) && $mubUserContact->save(false))
                    {
                        $success = $this->updateUserRecord($postData);
                        if($success)
                        {
                            return $this->goBack('/site/profile');
                        }
                    }
                }
                p([$mubUser->getErrors(),$mubUserContact->getErrors()]);
            }
            return $this->render('profile');
        }
    }

    public function actionHistory()
    {
        $mubUserId = \app\models\User::getMubUserId();
        $scheduledModel = new \app\models\ScheduledVisits();
        $mubUserModel = new \app\models\MubUser();
        $bookingModel = new \app\models\Booking();
        $currentUser = $mubUserModel::findOne($mubUserId);
        $scheduledDetails = $scheduledModel::find()->where(['mub_user_id' => $mubUserId,'del_status' => '0','status' => 'scheduled'])->all();
        $bookingDetails = $bookingModel::find()->where(['mub_user_id' => $mubUserId,'del_status' => '0'])->all(); 
        return $this->render('history',['currentUser' => $currentUser,'scheduledDetails' => $scheduledDetails , 'bookingDetails' => $bookingDetails]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'admin';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack('/mub-admin/');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }


   
    public function actionClientLogin()
    {
        if (\Yii::$app->request->isAjax){
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if(\Yii::$app->request->post())
            {
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                   return $this->redirect(Yii::$app->request->referrer);
                }
                else
                {
                    return $this->renderAjax('signin', [
                        'model' => $model,
                    ]);
                }    
            }
            //case the normal request for form
            return $this->renderAjax('signin', [
                'model' => $model,
            ]);
        }
        p('Nice Try!! :D');
    }

    public function actionClientRegister()
    {    
            $model = new ClientSignup();
             
            if ($model->load(Yii::$app->request->post())) 
            {      
                if ($user = $model->signup()) 
                {
                    if (Yii::$app->getUser()->login($user))
                    {
                    $mubUserId = \app\models\User::getMubUserId();
                    $mubUserModel = new \app\models\MubUser();
                    $mubUser = $mubUserModel::findOne($mubUserId);
                    $mubUserContact = $mubUser->mubUserContacts;

                        return $this->goBack('/site/payment');
                    }
                }
                else
                {
                   $model->addError('username','This Username Already Exists in Database');
                   return $this->render('client',['model' => $model]);
                }
            }
            return $model->getErrors();
    }

     public function actionPayment()
    {
        $this->view->params['page']='payment';
        return $this->render('payment');
    }

    public function actionClientRegisterValidate()
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new ClientSignup();
            $model->load(Yii::$app->request->post());
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        $mubUser = new \app\models\MubUser();
        $mubUserId = \app\models\User::getMubUserId();  
        $currentRole = $mubUser::findOne($mubUserId)['role'];
            if($currentRole == 'client') 
            {
             Yii::$app->user->logout();
                return $this->goHome(); 
            }
            else
            {
            Yii::$app->user->logout();
            return $this->redirect('/');
            }
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
     public function actionContact()
    {
        $this->view->params['page'] = 'contact';
        // $mubUserModel = new \app\models\MubUser();
        $contactMail = new \app\models\ContactMail();
        $params = \Yii::$app->request->post();
        if(!empty($params) && $contactMail->load($params))
        {
            $this->layout = false;
            if($contactMail->save(false))
            {
                \Yii::$app->mailer->compose('contact',['contactMail' => $contactMail])
                    ->setFrom('info@mayaexpress.in')
                    ->setTo($contactMail->email)
                    ->setCc('info@mayaexpress.in')
                    ->setSubject('OSMSTAYS Contact Info')
                    ->setBcc('praveen@makeubig.com','MakeUBIG ADMIN')
                    ->send();    
                return true;
            }
            else
            {
                p($contactMail->getErrors());
            }
                    
        }
        return $this->render('contact', [
        
            'contactMail' => $contactMail,
        ]);

    }
    public function actionRegister()
    {
        $this->layout = 'admin';
        $model = new SignupForm();
        $state = new \app\models\State();
        $allStates = $state->getAll('state_name');
        $where = ['del_status' => '0','active' => '1'];
        $activeStates = $state->getAll('state_name',$where);
        
        if ($model->load(Yii::$app->request->post())) 
        {      
            if ($user = $model->signup()) 
            {
                if (Yii::$app->getUser()->login($user))
                {

                $mubUserId = \app\models\User::getMubUserId();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($mubUserId);
                $mubUserContact = $mubUser->mubUserContacts;
                \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                    ->setFrom('info@mayaexpress.in')
                    ->setTo($mubUserContact->email)
                    ->setSubject('Your Profile Created')
                    ->setCc('info@mayaexpress.in')
                    ->setBcc('praveen@makeubig.com','MakeUBIG ADMIN')
                    ->send();
                return $this->goBack('/mub-admin/');
                    
                }
            }
            else
            {
                p('here');
            }
        }
        return $this->render('register', [
            'model' => $model,
            'allStates' => $allStates,
            'activeStates' => $activeStates
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */

     public function actionClient()
    {
        $this->view->params['page']='client';
        return $this->render('client');
    }

    public function getUserRole()
    {
       $id = Yii::$app->getUser()->id;
       $userModel = new \app\models\User();
       $user = $userModel::find()->where(['id' => $id])->one();
       $username = $user->username;
       $mubUserModel = new \app\models\MubUser();
       $user = $mubUserModel::find()->where(['username' => $username])->one();
       $role = $user->role;
       return $role;
    }

}

