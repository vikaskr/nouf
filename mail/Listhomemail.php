<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title>Passion - Responsive Email Template</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  
  <link href='http://fonts.googleapis.com/css?family=Raleway:600,700,400' rel='stylesheet' type='text/css'>
  
  <!--
    **********************************************
         Template by => http://pixelhint.com
    **********************************************
  -->
  
  <!--  General CSS  -->
  <style type="text/css">
    html{
        width: 100%; 
    }

    body{
      width: 100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      mso-margin-top-alt:0px; 
      mso-margin-bottom-alt:0px; 
      mso-padding-alt: 0px 0px 0px 0px;
      background: #E7E7E7;
    }

    p,h1,h2,h3,h4{
      margin-top:0;
      margin-bottom:0;
      padding-top:0;
      padding-bottom:0;
    }

    table{
      font-size: 14px;
      border: 0;
    }

    img{
      border: none!important;
    }
  </style>

  <!--  Responsive CSS  -->
  <style type="text/css">
    @media only screen and (max-width: 800px) {
      body[yahoo] .quote_full_width {width:100% !important;}
      body[yahoo] .quote_content_width {width:90% !important;}
    }

    @media only screen and (max-width: 640px) {
      body[yahoo] .full_width {width:100% !important;}
      body[yahoo] .content_width{width:80% !important;}
      body[yahoo] .center_txt {text-align: center!important;}
      body[yahoo] .post_sep {width:100% !important; height:60px !important;}
      body[yahoo] .gal_sep {width:100% !important; height:40px !important;}
      body[yahoo] .gal_img {width:100% !important;}
      body[yahoo] .bb_space {height:90px !important;}
    
    }  
  </style>
</head>
<body style="margin: 0; padding: 0;" yahoo="fix">

  <!--  billboard  -->

  <!--  features section  -->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#fbfbfb" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
      <!--  spacing  -->
      <tr>
        <td width="100%" height="100">&nbsp;</td>
      </tr>
      <!--  end spacing  -->

      <tr>
        <td>
          <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="text-align:center;" class="content_width">
            <!--  section title  -->
            <tr>

              <td style="color: #414d5f; font-family: 'Raleway', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: 700; letter-spacing:.5px; text-transform:uppercase;"><img src="<?php echo $_SERVER['HTTP_HOST'];?>/mail/img/mexpress.png"><br><br>Your Order</td>
            </tr>
            <!--  end section title  -->

            <!--  spacing  -->
            <tr>
              <td width="100%" height="20">&nbsp;</td>
            </tr>        
            <!--  end spacing  -->

            <!--  vertical separator  -->
 
        </td>
      </tr>

  </table>

  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
    
    <tr>
      <td>
    
        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="content_width">
          <tr>
            <td width="100%" height="40"></td>
          </tr>
          <!--  end spacing  -->

          <!--  content  -->
          <tr>
            <td>
              <!--  copyrights  -->
              <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full_width center_txt">
                <tr>
                  <td style="color: #414d5f; font-family: 'Raleway', Helvetica, Arial, sans-serif; font-size: 12px; letter-spacing:.5px; font-weight: 400;">
                    © 2018 <a href="http://pixelhint.com" target="_blank" style="color: #414d5f; font-weight: 600; text-decoration:none;">food.mayaexpress.com</a>. All Rights Reserved
                  </td>
                </tr>
              </table>
              <!--  end copyrights  -->

              <!--  spacing  -->
              <table height="40" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full_width">
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table>
              <!--  end spacing  -->

              <!--  social media  -->
              <table align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full_width center_txt">
                <tr>
                  <td>
                    <a href="http://facebook.com/pixelhint" target="_blank" style="color: #414d60; text-decoration:none;"><img src="mail/img/facebook.png" width="7" height="12" alt="" title="" border="0" style="border:0; display:inline_block;"/></a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="http://twitter.com/pixelhint" target="_blank" style="color: #414d60; text-decoration:none;"><img src="mail/img/twitter.png" width="14" height="10" alt="" title="" border="0" style="border:0; display:inline_block;"/></a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="http://google.com" target="_blank" style="color: #414d60; text-decoration:none;"><img src="mail/img/gplus.png" width="6" height="12" alt="" title="" border="0" style="border:0; display:inline_block;"/></a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="http://linkedin.com" target="_blank" style="color: #414d60; text-decoration:none;"><img src="mail/img/linkedin.png" width="11" height="12" alt="" title="" border="0" style="border:0; display:inline_block;"/></a>
                  </td>
                </tr>
              </table>
              <!--  end social media  -->
            </td>
          </tr>
          <!--  end content  -->

          <!--  spacing  -->
          <tr>
            <td width="100%" height="40">&nbsp;</td>
          </tr>
          <!--  end spacing  -->
        </table>
        <!--  end footer bottom block  -->
      </td>
    </tr>
  </table>
  <!--  end footer  -->

</body>
</html>