<?php

namespace app\migrations;
use app\commands\Migration;

class m170512_105352_alter_state extends Migration
{
    public function safeUp()
    {
        $this->addColumn('state','state_slug', 'VARCHAR(64) AFTER state_name');
        $state = new \app\models\State();
        $allStates = $state::find()->where(['del_status' => '0'])->all();
        foreach ($allStates as $stateId => $state) 
        {
            $state->state_slug = strtolower(str_replace(' ','_',$state->state_name));
            if(!$state->save(false))
            {
                p($state->getErrors());
            }
        }
    }

    public function getTableName()
    {
        return 'state';
    }

    public function getKeyFields()
    {
        return ['state_slug' => 'state_slug'];
    }

    public function getFields()
    {
        return [];
    }

    public function safeDown()
    {
      
    }
}
