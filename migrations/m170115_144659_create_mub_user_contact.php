<?php

namespace app\migrations;
use app\commands\Migration;

class m170115_144659_create_mub_user_contact extends Migration
{
    public function getTableName()
    {
        return 'mub_user_contact';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $mubUser = new \app\models\MubUser();
        $allUsers = $mubUser::find()->all();
        foreach ($allUsers as $user) 
        {
            $mubUserContacts = new \app\models\MubUserContact();
            $mubUserContacts->mub_user_id = $user->id;
            $mubUserContacts->email = 'admin@mub.com';
            $mubUserContacts->state = 'Delhi';
            $mubUserContacts->city = 'Delhi';
            $mubUserContacts->country = 'India';
            $mubUserContacts->pin_code= "1100089";
            $mubUserContacts->mobile = "0987654321";
            $mubUserContacts->landline = '1234567890';
            $mubUserContacts->address = "House No:, Street, City";
            $mubUserContacts->address_line_2 = 'admin@mub.com';
            $mubUserContacts->name_on_certificate = 'kijnmki';
            $mubUserContacts->course_location= "course_location";
            $mubUserContacts->courses = "0987654321";
            $mubUserContacts->other_courses = '1234567890';
            $mubUserContacts->get_to_know = "House No:, Street, City";
            if($mubUserContacts->save())
            {
                echo 'created user admin Contact \n';
            }
            else
            {
                p($mubUserContacts->getErrors());
            }
        }
    }

    public function getKeyFields()
    {
        return [
            'mub_user_id'  =>  'mub_user_id',
            'city' => 'city',
            'mobile' => 'mobile',
            'landline' => 'landline'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->defaultValue(NULL),
            'email' => $this->string(50)->notNull(),
            'mobile' => $this->string(100)->notNull(),
            'landline' => $this->string(100)->defaultValue(NULL),
            'work_phone' => $this->string(100)->defaultValue(NULL),
            'city' => $this->string(50)->notNull(),
            'state' => $this->string(50)->notNull(),
            'country' => $this->string(50)->notNull(),
            'pin_code' => $this->string(100)->defaultValue(NULL),
            'address' => $this->string()->defaultValue(NULL),
            'address_line_2' => $this->string()->defaultValue(NULL),
            'name_on_certificate' => $this->string(50)->notNull(),
            'course_location' => $this->string(255)->notNull(),
            'courses' => $this->string(255)->notNull(),
            'other_courses' => $this->string(255)->defaultValue(NULL),
            'get_to_know' => $this->string(255)->defaultValue(NULL),
            'lat' => $this->string(255)->defaultValue(NULL),
            'long' => $this->string(255)->defaultValue(NULL),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
