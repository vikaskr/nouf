<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact_mail".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $subject
 */
class ContactMail extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_mail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'subject'], 'required'],
            [['name', 'email', 'subject'], 'string'],
            ["email" ,'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'subject' => 'Message',
        ];
    }
}
