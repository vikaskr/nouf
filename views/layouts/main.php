<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;
use app\assets\AppAsset;
use app\models\MubUser;

AppAsset::register($this);

 $this->registerMetaTag([
      'title' => 'og:title',
      'content' =>'OSMSTAYS Property', 
   ]);

   $this->registerMetaTag([
      'app_id' => 'fb:app_id',
      'content' => '892362904261944'
   ]);

   $this->registerMetaTag([
      'type' => 'og:type',
      'content' => 'article'
   ]);

   $this->registerMetaTag([
      'url' => 'og:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
      $this->registerMetaTag([
         'image' => 'og:image',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo1.jpg',
      ]);
   $this->registerMetaTag([
      'description' => 'og:description',
      'content' => 'Looking for a house on rent in Gurgaon? OSMSTAYS provide fully furnished/semi-furnished flats at an Affordable rate with no brokerage and no lock-in period.'
   ]);

/*close facebook meta tag*/

/*twitter meta tag*/

   $this->registerMetaTag([
      'card' => 'twitter:card',
      'content' => "summury"
   ]);
   $this->registerMetaTag([
      'site' => "twitter:site",
      'content' => "@publisher_handle"
   ]);
   
   $this->registerMetaTag([
      'title' => 'twitter:title',
      'content' => 'OSMSTAYS Property'
   ]);

   $this->registerMetaTag([
      'description' => 'twitter:description',
      'content' => 'Looking for a house on rent in Gurgaon? OSMSTAYS provide fully furnished/semi-furnished flats at an Affordable rate with no brokerage and no lock-in period.' 
   ]);

   $this->registerMetaTag([
      'creater' => 'twitter:creater',
      'content' => '@author_handle' 
   ]);

   $this->registerMetaTag([
      'url' => 'twitter:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
   $this->registerMetaTag([
         'image' => 'twitter:image:src',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo1.jpg',
      ]);
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Nouf Yoga Acedemy</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="/images/favicon.png" type="/images/png" sizes="16x16">
   <!--  <link rel="stylesheet" type="text/css" href="css/vendor.bundle.css">
    <link rel="stylesheet" type="text/css" href="css/style.css"> -->
 <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


<!-- <script>

  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-104726724-1');
</script> -->

<body class="site-body style-v1">
<?php $this->beginBody() ?>
<header class="site-header header-s1 is-transparent is-sticky">
        <!-- Topbar -->
        <div class="topbar">
            <div class="container">
                <div class="row">
                    <div class="top-aside top-left">
                        <ul class="top-nav">
                            <li><a href="#">On Press</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Our Offices</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="top-aside top-right clearfix">
                        <ul class="top-contact clearfix">
                            <li class="t-email t-email1">
                                <em class="fa fa-envelope-o" aria-hidden="true"></em>
                                <span><a href="mailto:noufacademy@gmail.com">noufacademy@gmail.com</a></span>
                            </li>
                            <li class="t-phone t-phone1">
                                <em class="fa fa-phone" aria-hidden="true"></em>
                                <span>011 - 49842280</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- #end Topbar -->
        <!-- Navbar -->
        <div class="navbar navbar-primary">
            <div class="container">
                <!-- Logo -->
                <a class="navbar-brand" href="/">
                    <h2 style="color: #eca116;">Nouf Yogashala</h2>
                </a>
                <!-- #end Logo -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainnav" aria-expanded="false">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Q-Button for Mobile -->
                    <div class="quote-btn"><a class="btn" href="/site/client">Sign in</a></div>
                </div>
                <!-- MainNav -->
                <nav class="navbar-collapse collapse" id="mainnav">
                    <ul class="nav navbar-nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="#">About Us </a></li>
                        <li><a href="#">Registry </a></li>
                        <li><a href="#">Teacher Training Course </a></li>
                        <li><a href="#">Teachers </a></li>
                        <li><a href="#">Testimonials </a></li>
                        <li><a href="#">Photo Gallery </a></li>
                        <li><a href="#">Contact Us </a></li>
                       
                    </ul>
                </nav>     
                <!-- #end MainNav -->
            </div>
        </div>
        <!-- #end Navbar -->
        <!-- Banner/Slider -->

        <!-- #end Banner/Slider -->
    </header>
<?= $content ?>
            
    <!-- End Section -->

    <!-- Call Action -->
    <div class="call-action cta-small has-bg bg-primary" style="background-image: url('/images/banner.png');">
        <div class="cta-block">
            <div class="container">
                <div class="content row">

                    <div class="cta-sameline">
                        <h2>Have any Question?</h2>
                        <p>Get on touch with us on facebook / Instagram/ Twitter Search NAcademy</p>
                        <a class="btn btn-alt" href="#">Contact Us</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End Section -->

    <!-- Footer Widget-->
    <div class="footer-widget style-v2 section-pad-md">
        <div class="container">
            <div class="row">

                <div class="widget-row row">
                    <div class="footer-col col-md-3 col-sm-6 res-m-bttm">
                        <!-- Each Widget -->
                        <div class="wgs wgs-footer wgs-text">
                            <div class="wgs-content">
                                <p><img src="/images/logo.png" srcset="/images/logo2x.png 2x" alt=""></p>
                                <p>Our Courses are specially designed to expand the knowledge of yoga teachers & health practitioners to learn more in the field of Indian Medicine, Philosophy & Yoga. please request us at <a href="mailto:noufacademy@gmail.com">noufacademy@gmail.com</a> to know more...  </p>
                            </div>
                        </div>
                        <!-- End Widget -->
                    </div>
                    <div class="footer-col col-md-3 col-sm-6 col-md-offset-1 res-m-bttm">
                        <!-- Each Widget -->
                        <div class="wgs wgs-footer wgs-menu">
                            <h5 class="wgs-title">Our Courses</h5>
                            <div class="wgs-content">
                                <ul class="menu">
                                    <li><a href="#">Level1 Yoga teacher training course</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Widget -->
                    </div>
                    <div class="footer-col col-md-2 col-sm-6 res-m-bttm">
                        <!-- Each Widget -->
                        <div class="wgs wgs-footer wgs-menu">
                            <h5 class="wgs-title">Quick Links</h5>
                            <div class="wgs-content">
                                <ul class="menu">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Testimonial</a></li>
                                    <li><a href="#">Expert</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Widget -->
                    </div>

                    <div class="footer-col col-md-3 col-sm-6">
                        <!-- Each Widget -->
                        <div class="wgs wgs-footer">
                            <h5 class="wgs-title">Get In Touch</h5>
                            <div class="wgs-content">
                                <p>
                                    317,318.<br />
                                    Third floor, Plot no. 9 <br />
                                    Vikas Surya Galaxy, Sector 4,<br />
                                    Dwarka, New Delhi - 110075
                                <p><span>Toll Free</span>: 011 - 49842280<br>
                                    <span>Phone</span>: +91 - 8111811891 </p>
                                <ul class="social">
                                    <li><a href="#"><em class="fa fa-facebook" aria-hidden="true"></em></a></li>
                                    <li><a href="#"><em class="fa fa-twitter" aria-hidden="true"></em></a></li>
                                    <li><a href="#"><em class="fa fa-linkedin" aria-hidden="true"></em></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Widget -->
                    </div>

                </div><!-- Widget Row -->

            </div>
        </div>
    </div>
    <!-- End Footer Widget -->

    <!-- Copyright -->
     <div class="copyright style-v2">
    <div class="container">
      <div class="row">
      
        <div class="row">
          <div class="site-copy col-sm-7">
            <p>&copy; 2017 Nouf Academy. <a href="#">Policy</a></p>
          </div>
          <div class="site-by col-sm-5 al-right">
            <p>Template Made by <a href="http://makeubig.com/" target="_blank">MakeUBIG.</a></p>
          </div>
        </div>
                
      </div>
    </div>
  </div>
</body>
<?php $this->endBody();?>
</html>
<?php $this->endPage();?>
