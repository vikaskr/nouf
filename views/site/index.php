
        <!-- Banner/Slider -->
        <div id="slider" class="banner banner-slider slider-large carousel slide carousel-fade">
            <!-- Wrapper for Slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <!-- Set the first background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('images/slider-lg-a.jpg');">
                        <div class="banner-content">
                            <div class="container">
                                <div class="row">
                                    <div class="banner-text al-left pos-left light">
                                        <h2>Fit your life and budget<strong>.</strong></h2>
                                        <p>We provide independent advice based on established research methods, and our experts have in-depth sector knowledge.</p>
                                        <a href="#" class="btn">Learn more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <!-- Set the second background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('images/slider-lg-b.jpg');">
                        <div class="banner-content">
                            <div class="container">
                                <div class="row">
                                    <div class="banner-text al-left pos-left light">
                                        <h2>Expert Financial Advice<strong>.</strong></h2>
                                        <p>We help clients find ways to turn into actionable insights by embedding economics across their organization’s strategy.</p>
                                        <a href="#" class="btn">Learn more</a>
                                    </div>
                                </div>
                            </div>
                        </div>                  
                    </div>
                </div>
            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#slider" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- #end Banner/Slider -->
    </header>
    <!-- End Header -->
    
    <!-- Service Section -->
    <div class="section section-services">
        <div class="container">
            <div class="content row">
                <!-- Feature Row  -->
                <div class="feature-row feature-service-row row feature-s4 off-text boxed-filled boxed-w">
                    <div class="heading-box clearfix">
                        <div class="col-sm-3">
                            <h2 class="heading-section" style="color: #eaa515!important; padding-top: 15px; font-size: 29px;">WELCOME TO NOUF YOGASHALA</h2>
                        </div>
                        <div class="col-sm-8 col-sm-offset-1">
                            <span style="color: #fff;font-weight: 500;">NOUF YOGASHALA is the first official yoga organization in the Gulf Region, it was established in 2010 by Saudi yogcharinie Nouf Marwaai, the founder of the Saudi Arabia Yoga School 2008, </span>
                        </div>
                    </div>
                    
                        <!-- End Feature box -->
                    </div>                  
                </div>
                <!-- Feture Row  #end -->

            </div>
        </div>
    </div>
    <!-- End Section -->

    <!-- Content -->
    <div class="section section-content section-pad">
        <div class="container">
            <div class="content row">

                <div class="row row-vm">
                    <div class="col-md-6 res-m-bttm">
                        <h5 class="heading-sm-lead">About us</h5>
                        <h2 class="heading-section">Who we are</h2>
                        <p>We are Finance Corp, We provide Finance consulting from 30 years.</p>
                        <p>Accumsan est in tempus etos ullamcorper sem quam suscipit lacus maecenas tortor. Suspendisse gravida ornare non mattis velit rutrum modest sed do eiusmod tempor incididunt ut labore et dolore. </p>
                        <p>We have one of the philo sophia nec mei maiorum appell antur. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus egestas varius penatibus.</p>
                    </div>  
                    <div class="col-md-5 col-md-offset-1">
                        <img class="no-round" src="images/photo-home-a.jpg" alt="">
                    </div>
                </div>
                
            </div>  
        </div>
    </div>
    <!-- End Content -->

    <!-- Testimonials -->
  
    <!-- End Section -->

    
    <!-- End Content -->
    <!-- Teams -->
    <div class="section section-teams section-pad bdr-bottom">
        <div class="container">
            <div class="content row">

                <div class="col-md-offset-2 col-md-8 center">
                    <h5 class="heading-sm-lead">Nouf Yogashala</h5>
                    <h2 class="heading-section">Our Courses</h2>
                </div>
                <div class="gaps"></div>
                <div class="team-member-row row">
                    <div class="col-md-3 col-sm-6 col-xs-6 even">
                        <!-- Team Profile -->
                        <div class="team-member">
                            <div class="team-photo">
                                <img alt="" src="images/team-a.jpg">
                            </div>
                            <div class="team-info center">
                                <h4 class="name">Robert Miller</h4>
                                <p class="sub-title">Managing Director &amp; CEO</p>
                               <br/> <a class="btn btn-alt" href="#">Read More</a>
                            </div>

                        </div>
                        <!-- Team #end -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 odd">
                        <!-- Team Profile -->
                        <div class="team-member">
                            <div class="team-photo">
                                <img alt="" src="images/team-b.jpg">
                            </div>
                            <div class="team-info center">
                                <h4 class="name">Stephen Everett</h4>
                                <p class="sub-title">Chief Financial Officer</p>
                               <br/> <a class="btn btn-alt" href="#">Read More</a>
                            </div>
                        </div>
                        <!-- Team #end -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 even">
                        <!-- Team Profile -->
                        <div class="team-member">
                            <div class="team-photo">
                                <img alt="" src="images/team-c.jpg">
                            </div>
                            <div class="team-info center">
                                <h4 class="name">Philip Hennessy</h4>
                                <p class="sub-title">Senior Tax Specialist</p>
                               <br/> <a class="btn btn-alt" href="#">Read More</a>
                            </div>
                        </div>
                        <!-- Team #end -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 odd">
                        <!-- Team Profile -->
                        <div class="team-member">
                            <div class="team-photo">
                                <img alt="" src="images/team-d.jpg">
                            </div>
                            <div class="team-info center">
                                <h4 class="name">Robert Miller</h4>
                                <p class="sub-title">Chief Financial Advisor</p>
                               <br/> <a class="btn btn-alt" href="#">Read More</a>
                            </div>
                        </div>
                        <!-- Team #end -->
                    </div>
                </div><!-- TeamRow #end -->
            </div>
        </div>
    </div>          
    <!-- End Section -->
         <div class="section section-quotes section-pad" style="background-color: #D280A4; margin-top: 50px!important; padding-bottom: 40px!important;">
        <div class="container">
            <div class="content row">
            
                <div class="col-md-offset-2 col-md-8 center">
                    <h5 class="heading-sm-lead">The People Say</h5>
                    <h2 class="heading-section">Testimonials</h2>
                </div>
                <div class="gaps"></div>
                <div class="testimonials">
                    <div id="testimonial" class="quotes-slider col-md-8 col-md-offset-2">
                        <div class="owl-carousel loop has-carousel" data-items="1" data-loop="true" data-dots="true" data-auto="true">
                            <div class="item">
                                <!-- Each Quotes -->
                                <div class="quotes">
                                    <div class="quotes-text center">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo nemo enim ipsam.</p>
                                    </div>
                                    <div class="profile">
                                        <img src="image/profile-img.jpg" alt="">
                                        <h5>Maria jaqline</h5>
                                        <h6>CEO, Company Name</h6>
                                    </div>
                                </div>
                                <!-- End Quotes -->
                            </div>
                            <!-- End Slide -->
                            <!-- Each Slide -->
                            <div class="item">
                                <!-- Each Quotes -->
                                <div class="quotes">
                                    <div class="quotes-text center">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</p>
                                    </div>
                                    <div class="profile">
                                        <img src="image/profile-img.jpg" alt="">
                                        <h5>Maria jaqline</h5>
                                        <h6>CEO, Company Name</h6>
                                    </div>
                                </div>
                                <!-- End Quotes -->
                            </div>
                        </div>
                        <!-- End Slide -->
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- Latest News -->
    <div class="section section-news section-pad" style="padding-bottom: 60px;">
        <div class="container">
            <div class="content row">
                
                <h5 class="heading-sm-lead center">Nouf Yogashala</h5>
                <h2 class="heading-section center">Our Teacher</h2>

                <div class="row">
                    <!-- Blog Post Loop -->
                    <div class="blog-posts">
                        <div class="post post-loop col-md-3">
                            <div class="post-thumbs">
                                <a href="news-details.html"><img alt="" src="images/post-thumb-a.jpg"></a>
                            </div>
                            <div class="post-entry">
                                <div class="post-meta"><span class="pub-date">15, Aug 2017</span></div>
                                <h2><a href="news-details.html">Income Increase Shows the Recovery Is Very Much Real</a></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat...</p>
                            </div>
                               <br/> <a class="btn btn-alt" href="#">Read More</a>
                        </div>
                        <div class="post post-loop col-md-3">
                            <div class="post-thumbs">
                                <a href="news-details.html"><img alt="" src="images/post-thumb-a.jpg"></a>
                            </div>
                            <div class="post-entry">
                                <div class="post-meta"><span class="pub-date">15, Aug 2017</span></div>
                                <h2><a href="news-details.html">Income Increase Shows the Recovery Is Very Much Real</a></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat...</p>
                                </div>
                                <br/> <a class="btn btn-alt" href="#">Read More</a>
                        </div>
                        <div class="post post-loop col-md-3">
                            <div class="post-thumbs">
                                <a href="news-details.html"><img alt="" src="images/post-thumb-b.jpg"></a>
                            </div>
                            <div class="post-entry">
                                <div class="post-meta"><span class="pub-date">04, Jul 2017</span></div>
                                <h2><a href="news-details.html">An Economics Nobel awarded for Examining Reality</a></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat...</p>
                                </div>
                                <br/> <a class="btn btn-alt" href="#">Read More</a>
                        </div>
                        <div class="post post-loop col-md-3">
                            <div class="post-thumbs">
                                <a href="news-details.html"><img alt="" src="images/post-thumb-c.jpg"></a>
                            </div>
                            <div class="post-entry">
                                <div class="post-meta"><span class="pub-date">26, Dec 2016</span></div>
                                <h2><a href="news-details.html">Maybe Supply-Side Economics Deserves a Second Look</a></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat...</p>
                                </div>
                                <br/> <a class="btn btn-alt" href="#">Read More</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>


    <!-- End Section -->
    
    <!-- Client logo -->
 