
<?php 
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
$mubUserId = \app\models\User::getMubUserId();
$mubUserModel = new \app\models\MubUser();
$currentUser = $mubUserModel::findOne($mubUserId);
$userDetails = $currentUser;
$contactDetails = $currentUser->mubUserContacts;

?>
<br/>
<section class="subheader">
  <div class="container">
    <h1>Profile</h1>
    <div class="breadcrumb right">Home <i class="fa fa-angle-right"></i> <a href="#" class="current">Profile</a></div>
    <div class="clear"></div>
  </div>
</section>
<section class="module favorited-properties margi">
  <div class="container margi3 margi2 colr2">
  <div class="row" style="margin: 2em;">
    <div class="col-lg-6 col-md-6">
        <div class="row">
          <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['layout' => 'horizontal','options' => ['method' => 'POST'],'action' => ['/site/profile']]); ?>
            
            <div class="col-md-12" style="padding: 10px;"><?= $form->field($currentUser, 'first_name')->textInput(['class' => 'form-block']);?></div>
            <div class="col-md-12" style="padding: 10px;"><?= $form->field($currentUser, 'last_name')->textInput(['class' => 'form-block']);?></div>
            <div class="col-md-12" style="padding: 10px;"><?= $form->field($currentUser, 'username')->textInput(['class' => 'form-block','readonly' => true]);?></div>
            <div class="col-md-12" style="padding: 10px;"><?= $form->field($currentUser, 'password')->passwordInput(['class' => 'form-block']);?></div>
            <div class="col-md-12" style="padding: 10px;"><?= $form->field($contactDetails, 'mobile')->textInput(['class' => 'form-block','maxlength' => 10]);?></div>
            
            <div class="col-md-12" style="padding: 10px;"><?= $form->field($currentUser,'id')->hiddenInput()->label(false);?></div><br/>
          <div class="col-md-8"><button type="submit" class="button button-icon" value="Update" ><i class="fa fa-check"></i>Save Changes</button></div>
           <?php ActiveForm::end(); ?>
          </div>
        </div><!-- end row -->

    </div>
    <div class="col-lg-6 col-md-6">
            <div class="col-md-12" style="padding: 10px;"><?= $form->field($contactDetails, 'email')->textInput(['class' => 'form-block','readonly' => true]);?></div>
            <div class="col-md-12" style="padding: 10px;"><?= $form->field($contactDetails, 'address')->textarea(['autofocus' => true,'class' => 'form-block']);?></div>
           
     </div> 
  </div><!-- end row -->

  </div><!-- end container -->
</section>