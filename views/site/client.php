<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\ClientSignup;
$model = new ClientSignup();
?>
<style type="text/css">
</style>

<div class="container">
<div class="login-grids">
    <div class="login">
        <div class="login-right">
            <?php $form = ActiveForm::begin(['layout' => 'horizontal','enableAjaxValidation' => true,'validationUrl' => ['site/client-register-validate'],'options' => ['id' => 'frontend-signup','method' => 'POST','data-pjax' => true],'action' => ['/#']]); ?>
                <div class="row">
                <div class="col-md-5"></div>
                <div class="col-md-4 text-c" style="margin-top: 8em; margin-bottom: 2em;"><h2>Register</h2></div>
                </div>
                <div class="row"><?= $form->field($model, 'first_name')->textInput(['placeholder' => " Your First Name"]);?></div>
                <div class="row"><?= $form->field($model, 'last_name')->textInput(['placeholder' => " Your Last Name"]);?></div>
                <div class="row"><?= $form->field($model, 'mobile')->textInput(['placeholder' => " Your Mobile"]);?></div>
                <div class="row"><?= $form->field($model, 'email')->textInput(['placeholder' => " Your Email "]);?></div>
                <div class="row"><?= $form->field($model, 'name_on_certificate')->textInput(['placeholder' => " Your Name On Certificate"])->label('Name On Certificate');?></div>
                <div class="row"><?= $form->field($model, 'address')->textInput(['placeholder' => " Your Address"])->label('Address Line 1');?></div>
                <div class="row"><?= $form->field($model, 'address_line_2')->textInput(['placeholder' => " Your Address 2"])->label('Address Line 2');?></div>
                <div class="row"><?= $form->field($model, 'course_location')->textInput(['placeholder' => " Your Course Location"])->label('Course Location');?></div>
                <div class="row"><?= $form->field($model, 'courses')->dropDownList(['courses' => 'Level 1 Yoga Teacher Training'], ['prompt' => 'Select A Course']);?></div>
                <div class="row"><?= $form->field($model, 'other_courses')->textInput(['placeholder' => " Your Other Courses"])->label('Other Courses');?></div>
                <div class="row"><?= $form->field($model, 'get_to_know')->textInput(['placeholder' => "How get you know"])->label('How get you know');?></div>

                <?= $form->field($model,'country')->hiddenInput(['value' => 'India'])->label(false);?>
                <br/>
                <div class="row">
                <div class="col-md-5"></div><div class="col-md-7"><input type="submit" value="Register Now" style="margin-top: -2em!important;color: #fff; margin-bottom: 0.2em; width: 150px; background: #eaa515;; padding: 5px;"></div></div><br/>
             <?php ActiveForm::end(); ?>
        </div>
        <div class="clearfix"></div>                                
    </div>
    <!-- <div class="row">
    <div class="col-md-4"></div><div class="col-md-8"><p style="color: #000!important;">By logging in you agree to our <a href="#" style="color: #6298c7!important;" target="_blank">Terms and Conditions</a> and <a href="#" style="color: #6298c7!important;" target="_blank">Privacy Policy</a></p></div><br><br>
    </div> -->
</div>
</div>

