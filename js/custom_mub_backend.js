var setModelAttribute = function(model,attribute,value,id)
  {
      var dataArray = {model:model,attribute:attribute,value:value,id:id};
      $.ajax({
       type:'POST',
       url:"/mub-admin/dashboard/set-attribute",
       data:dataArray,
          success: function(result){
              if(result)
              {
                  location.reload();
              }
          }
      }); 
  };

  var changeUserStatus = function(field,val,id)
    {
        var positive = confirm('Are you sure you want to change this user\'s status?');
        if(positive)
        {
          var models = [{name:'\\app\\models\\User',status:val},{name:'\\app\\models\\MubUser',status:val}];
          for (var i = models.length - 1; i >= 0; i--) {
            setModelAttribute(models[i].name,'status',val,id);  
          }
        }
    };

  function getFormattedPartTime(partTime){
     if (partTime < 10)
        return "0"+partTime;
     return partTime;
  }

	$('#mub-state').on('change',function(){
    var selected = $("#mub-state option:selected").val();
    $.ajax({
         type:'POST',
         url:"/mub-admin/dashboard/get-city",
         data:{stateId:selected},
        success: function(result){
            $('#mubusercontact-city').children("option").remove();
            $('#mubusercontact-city').prop("disabled", false);
            $('#mubusercontact-city').append(new Option('Select', ''));
        $.each(result.result, function (i, item) {
          $('#mubusercontact-city').append(new Option(item.city_name, item.id));
        });
        }
      })
    }); 
  
   $('#bed-available').on('change',function(){
       var selected = $("#bed-available option:selected").val();
       if(selected == 1)
       {
           var date = new Date();
           var month = date.getMonth()+1;
           var str = date.getFullYear() + "-" + getFormattedPartTime(month) + "-" + getFormattedPartTime(date.getDate());
           $('#bed-availability_date').val(str);
           $('.field-bed-availability_date').css('display','none');
       }else
       {
           $('.bed-availability_date').css('disable','false');
           $('.field-bed-availability_date').css('display','block');
       }  
   });
   

$('.del-image').on('click',function()
 {
    var imageName = $(this).attr('id');
   var stateId = $('#current-state-id').val();
     $.ajax({
         type:'GET',
         url:"/mub-admin/hotels/restaurant/image-delete?name="+imageName+'&state='+stateId,
         success: function(result)
         {
          alert(result);
          location.reload();        
         }
        }); 
});

$(document).on("click", "#confirm-order", function(event){
   var bookingid = $(this).val()
     $.ajax({
         type:'GET',
         url:"/site/confirm-order",
         data:{'bookingid':bookingid},
         success: function(result)
          {   
           location.reload();   
          }
      });
});

$(document).on("click", "#order-delete", function(event){
   var booking_id = $(this).val();
     $.ajax({
         type:'GET',
         url:"/site/order-delete",
         data:{'booking_id':booking_id},
         success: function(result)
          {    
           location.reload();   
          }
      });
});

function initialize() {
 var options = {
 types: ['(regions)'],
 componentRestrictions: {country: "in" }
 };
 var places = document.getElementById("restaurant-sa_b");
 var autocomplete = new google.maps.places.Autocomplete(places, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        //Initializing all the variables
    var stateName = null;
    var pincode = null;
    var locality = null;
    var place = autocomplete.getPlace();
    console.log(JSON.stringify(place.address_components));
        for (var i = place.address_components.length - 1; i >= 0; i--) 
       {
           if(place.address_components[i].types[0] == 'administrative_area_level_1')
           {
               stateName = place.address_components[i].long_name;
           }

           if(place.address_components[i].types[0] == 'postal_code')
           {
               pincode = place.address_components[i].long_name;
           }
           if(place.address_components[i].types[0] == 'sublocality_level_1')
           {
               locality = place.address_components[i].long_name;
           }
       }
   
   var components = this.getPlace().address_components,city='n/a';
   if(components){
        for(var c=0;c<components.length;++c){
        console.log(components[c].types.join('|'))
         if(components[c].types.indexOf('locality')>-1
              &&
            components[c].types.indexOf('political')>-1
           ){
           city=components[c].long_name;
           break;
         }
       }
     }
    console.log(JSON.stringify(place));
    var lat = place.geometry['location'].lat();
    var lng = place.geometry['location'].lng();
    stateName = stateName.replace(/ /g,'-');
    stateName = stateName.toLowerCase();
    var currentStateName = document.getElementById("current-state-name").value;
    if(currentStateName !== stateName)
    {
        window.confirm("Please Check ! You are adding Property of " + stateName + " in "+ currentStateName);
    }
    document.getElementById("city_name").value = city;
    document.getElementById("lat").value = lat;
    document.getElementById("long").value = lng;        
    document.getElementById('state_name').value = stateName;
    if(pincode !== null)
    {
        document.getElementById('pincode').value = pincode;
    }
    if(locality !== null)
    {
        document.getElementById('locality').value = locality;    
    }
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
var places = document.getElementById("restaurant-sa_b");

if(places !== null)
{
    google.maps.event.addDomListener(places, 'keydown', function(e) { 
        if (e.keyCode == 13) { 
            e.preventDefault(); 
        }
      });    
}
